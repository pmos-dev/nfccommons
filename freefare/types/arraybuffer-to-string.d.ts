declare module 'arraybuffer-to-string' {
	namespace ArrayBufferToString {}

	function ArrayBufferToString(buffer: Buffer, encoding?: string): string;
	
	export = ArrayBufferToString;
}
