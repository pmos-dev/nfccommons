declare module 'freefare' {
	import {
			Tag, 
			MifareUltralightTag, 
			MifareClassicTag,
			MifareDesfireTag,
			Device
	} from 'freefare-pseudo';
	
	namespace Freefare {}
	
//	class Tag {
//		constructor(cppTag: string);
//
//		getType(): string;
//		getFriendlyName(): string;
//		getUID(): string;
//	}
//
//	class MifareUltralightTag extends Tag {
//		open(): Promise<void>;
//		close(): Promise<void>;
//		read(page: number): Promise<Buffer>;
//		write(page: number, buf: Buffer): Promise<void>;
//	}
//
//	class MifareClassicTag extends Tag {
//		open(): Promise<void>;
//		close(): Promise<void>;
//		authenticate(block: number, key: Buffer, keyType: string): Promise<void>;
//		read(block: number): Promise<Buffer>;
//		write(block: number, buf: Buffer): Promise<void>;
//		initValue(block: number, value: number, adr: string): Promise<void>;
//		readValue(block: number): Promise<number>;
//		incrementValue(block: number, amount: number): Promise<void>;
//		decrementValue(block: number, amount: number): Promise<void>;
//		restoreValue(block: number): Promise<void>;
//		transferValue(block: number): Promise<void>;
//	}
//
//	class MifareDesfireTag extends Tag {
//		open(): Promise<void>;
//		close(): Promise<void>;
//		authenticateDES(KeyNum: number, key: Buffer): Promise<void>;
//		authenticate3DES(KeyNum: number, key: Buffer): Promise<void>;
//		getApplicationIds(): Promise<number[]>;
//		selectApplication(aid: Buffer): Promise<void>;
//		getFileIds(): Promise<number[]>;
//		read(file: number, offset: number, length: number): Promise<Buffer>;
//		write(file: number, offset: number, length: number, data: Buffer): Promise<void>;
//	}
//
//	class Device {
//		name: string;
//
//		constructor(cppDevice: string);
//
//		open(): Promise<void>;
//		close(): Promise<void>;
//		listTags(): Promise<Tag[]>;
//		abort(): Promise<void>;
//	}

	class Freefare {
		constructor();
		
		listDevices(): Promise<Device[]>;
	}

	export = Freefare;
}
