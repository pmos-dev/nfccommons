import * as ab2str from 'arraybuffer-to-string';

import { INfcDesfireFile } from 'nfccommons-core';

import { NfcDevice } from '../classes/nfc-device';

import { NfcContinuousScannerDesfire } from './nfc-continuous-scanner-desfire';

export abstract class NfcContinuousScannerDesfireString extends NfcContinuousScannerDesfire<string> {
	constructor(
			device: NfcDevice,
			file: INfcDesfireFile,
			useCache: boolean = false
	) {
		super(device, file, useCache);
	}
	
	protected interpret(buffer: Buffer): string|undefined {
		const data: string = ab2str(buffer);
		return data;
	}
}
