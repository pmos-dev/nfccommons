import { CommonsOutput } from 'nodecommons-cli';

import { INfcDesfireFile } from 'nfccommons-core';
import { ENfcType } from 'nfccommons-core';

import { NfcDevice } from '../classes/nfc-device';
import { NfcTag } from '../classes/nfc-tag';
import { NfcDesfire } from '../classes/nfc-desfire';

import { NfcContinuousScanner } from './nfc-continuous-scanner';

export abstract class NfcContinuousScannerDesfire<T> extends NfcContinuousScanner<T> {
	constructor(
			device: NfcDevice,
			private readonly file: INfcDesfireFile,
			useCache: boolean = false
	) {
		super(ENfcType.MIFARE_DESFIRE, device, useCache);
	}
	
	protected abstract interpret(buffer: Buffer): T|undefined;

	protected async readTag(tag: NfcTag): Promise<T|undefined> {
		const desfire: NfcDesfire = tag as NfcDesfire;
		
		CommonsOutput.debug(`Selecting application: ${this.file.aid}`);
		const aids: number[] = await desfire.listApplications();
		
		const compare: number = (this.file.aid[0] * 256 * 256) + (this.file.aid[1] * 256) + this.file.aid[2];
		if (!aids.includes(compare)) throw new Error('No such application exists');
		
		if (!(await desfire.selectApplication(this.file.aid))) throw new Error('Unable to select application');

		switch (this.file.auth) {
			case 'des':
				CommonsOutput.debug('Authenticating using DES');
				if (!(await desfire.authenticateDES(this.file.keyNum, this.file.key))) throw new Error('Unable to authenticate');
				break;
			case '3des':
				CommonsOutput.debug('Authenticating using 3DES');
				if (!(await desfire.authenticate3DES(this.file.keyNum, this.file.key))) throw new Error('Unable to authenticate');
				break;
		}
		
		const files: number[] = await desfire.listFiles();
		if (!files.includes(this.file.file)) throw new Error('No such file exists');
		
		CommonsOutput.debug('Reading this.file');
		const read: Buffer = await desfire.read(this.file.file, this.file.offset, this.file.length);
		if (!read) throw new Error('Error reading file');

		return this.interpret(read);
	}
}
