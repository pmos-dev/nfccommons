import { CommonsAsync } from 'tscommons-async';
import { CommonsDebouncer } from 'tscommons-async';
import { CommonsSemaphore } from 'tscommons-async';

import { CommonsOutput } from 'nodecommons-cli';

import { ENfcType } from 'nfccommons-core';

import { NfcDevice } from '../classes/nfc-device';
import { NfcTag } from '../classes/nfc-tag';

export abstract class NfcContinuousScanner<T> {
	private readonly semaphore: CommonsSemaphore = new CommonsSemaphore();
	private abortFlag: boolean = false;
	private scanningEnabled: boolean = false;
	private cache: Map<string, T> = new Map<string, T>();
	
	constructor(
			private readonly tagType: ENfcType,
			private readonly device: NfcDevice,
			private readonly useCache: boolean = false
	) {}

	protected async abstract readTag(tag: NfcTag): Promise<T|undefined>;
	protected async abstract start(): Promise<void>;
	protected async abstract stop(): Promise<void>;
	protected async abstract handleData(uid: string, data: T): Promise<void>;

	public abort(force: boolean = false): void {
		try {
			if (force) this.device.abort();	// don't await, since might hang
		} catch (e) {
			CommonsOutput.error(e);
		} finally {
			this.abortFlag = true;
		}
	}
	
	public directlyClose(): void {
		try {
			this.device.close();	// don't await, since might hang
		} catch (e) {
			CommonsOutput.error(e);
		}
	}
	
	public setScanningEnabled(state: boolean): void {
		this.scanningEnabled = state;
	}
	
	public importCache(entries: Map<string, T>): void {
		for (const uid of entries.keys()) {
			this.cache.set(uid, entries.get(uid)!);
		}
	}
	
	public resetCache(): void {
		this.cache.clear();
	}
	
	public exportCache(): Map<string, T> {
		const entries: Map<string, T> = new Map<string, T>();
		for (const uid of this.cache.keys()) {
			entries.set(uid, this.cache.get(uid)!);
		}

		return entries;
	}
	
	public isAborted(): boolean {
		return this.abortFlag;
	}
	
	public async run(): Promise<void> {
		CommonsOutput.starting('Opening device');
		await this.device.open();
		try {
			const debouncer: CommonsDebouncer<T|undefined> = new CommonsDebouncer<T|undefined>(5000);
			
			await this.start();
			
			while (!this.abortFlag) {
				try {
					if (this.scanningEnabled) {
						await this.semaphore.claim(1000);
						try {
							const tags: NfcTag[] = (await this.device.listTags())
									.filter((tag: NfcTag): boolean => tag.type === this.tagType);
							
							if (tags.length === 0) {
								debouncer.debounce(undefined);
							} else {
								for (const tag of tags) {
									if (this.useCache && this.cache.has(tag.uid)) {
										CommonsOutput.debug(`Cache entry for ${tag.uid} exists. Using this`);
										try {
											const read: T = this.cache.get(tag.uid)!;
											
											const out: T|undefined = debouncer.debounce(read);
											if (out !== undefined) await this.handleData(tag.uid, out);
										} catch (e) {
											CommonsOutput.error(e);
										}
									} else {
										if (!(await tag.open())) continue;
										try {
											const read: T|undefined = await this.readTag(tag);
											
											const out: T|undefined = debouncer.debounce(read);
											if (out !== undefined) {
												await this.handleData(tag.uid, out);
											
												// do this after so that exceptions don't always get repeated rather than cached
												this.cache.set(tag.uid, out);
											}
										} catch (e) {
											CommonsOutput.error(e);
										} finally {
											await tag.close();
										}
									}
								}
							}
						} catch (e) {
							CommonsOutput.error(e);
						} finally {
							this.semaphore.release();
						}
					}
					
					await CommonsAsync.timeout(250);
				} catch (e) {
					CommonsOutput.error(e);
				}
			}
	
			await this.stop();
			//process.exit(0);
		} catch (e) {
			CommonsOutput.error(e);
		} finally {
			CommonsOutput.info('Closing device');
			await this.device.close();
		}
	}
}
