import * as LibFreefare from 'freefare';
import { Device, Tag } from 'freefare-pseudo';

import { CommonsNumber } from 'tscommons-core';

export enum EDummyType {
		DESFIRE
}

// tslint:disable-next-line:no-empty-interface
export interface IDummyTagDetails {}

export interface IDummyDesfireCardDetails extends IDummyTagDetails {
		aid: number;
		fileId: number;
		data: string;
}

abstract class DummyTag {
	abstract getType(): string;
	abstract getFriendlyName(): string;
	abstract getUID(): string;
	
	public async open(): Promise<void> {
		return;
	}

	public async close(): Promise<void> {
		return;
	}
	
	public asFreefare(): Tag {
		return this as Tag;
	}
}
	
class DummyDesfire extends DummyTag {
	private uid: string;
	
	constructor(
			private cardDetails: IDummyDesfireCardDetails
	) {
		super();
		
		this.uid = CommonsNumber.longRandom(9);
	}
	
	public getType(): string {
		return 'MIFARE_DESFIRE';
	}
	
	public getFriendlyName(): string {
		return 'Dummy desfire tag';
	}
	
	public getUID(): string {
		return this.uid;
	}

	public async getApplicationIds(): Promise<number[]> {
		return [ this.cardDetails.aid ];
	}

	public async selectApplication(_: Buffer): Promise<void> {
		return;
	}

	public async authenticateDES(_keyNum: number, _key: Buffer): Promise<void> {
		return;
	}

	public async authenticate3DES(_keyNum: number, _key: Buffer): Promise<void> {
		return;
	}

	public async getFileIds(): Promise<number[]> {
		return [ this.cardDetails.fileId ];
	}

	public async read(_file: number, _offset: number, _length: number): Promise<Buffer> {
		return Buffer.from(this.cardDetails.data);
	}

}

class DummyDevice {
	public name: string = 'Dummy NFC device';
	
	private tags: DummyTag[] = [];
	
	constructor(private type: EDummyType) {}
	
	public async open(): Promise<void> {
		return;
	}
	
	public async close(): Promise<void> {
		return;
	}
	
	public async abort(): Promise<void> {
		return;
	}

	public addTag(details: IDummyTagDetails, overrideType?: EDummyType): void {
		switch (overrideType || this.type) {
			case EDummyType.DESFIRE:
				this.tags.push(new DummyDesfire(details as IDummyDesfireCardDetails));
				break;
			default:
				throw new Error('Unknown dummy device type');
		}
	}
	
	public clearTags(): void {
		this.tags = [];
	}
	
	public async listTags(): Promise<Tag[]> {
		return this.tags
				.map((tag: DummyTag): Tag => tag as Tag);
	}
	
	public asFreefare(): Device {
		return this as Device;
	}
}

export class NfcDummy {
	private device: DummyDevice;
	
	constructor(private type: EDummyType) {
		this.device = new DummyDevice(this.type);
	}
	
	public addTag(cardDetails: IDummyDesfireCardDetails): void {
		this.device.addTag(cardDetails);
	}
	
	public clearTags(): void {
		this.device.clearTags();
	}
	
	public async listDevices(): Promise<Device[]> {
		return [ this.device.asFreefare() ];
	}
	
	public asFreefare(): LibFreefare {
		return this as LibFreefare;
	}
}
