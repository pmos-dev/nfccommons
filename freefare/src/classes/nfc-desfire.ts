import * as fs from 'fs';

import * as ab2str from 'arraybuffer-to-string';
import { MifareDesfireTag } from 'freefare-pseudo';

import { CommonsType } from 'tscommons-core';

import { CommonsOutput } from 'nodecommons-cli';

import { INfcDesfireFile, isINfcDesfireFile } from 'nfccommons-core';

import { NfcTag } from './nfc-tag';

export class NfcDesfire extends NfcTag {
	
	public static readDesfireFile(file: string): INfcDesfireFile {
		const data: string = fs.readFileSync(file, 'utf8');
		
		const parsed: any = JSON.parse(data);
		if (!isINfcDesfireFile(parsed)) throw new Error('Read file is not a valid INfcDesfireFile object');
		
		return parsed;
	}
	
	constructor(
			tag: MifareDesfireTag
	) {
		super(tag);
	}
	
	public async listApplications(): Promise<number[]> {
		try {
			const aids: any = await (this.tag as MifareDesfireTag).getApplicationIds();
			if (!CommonsType.isNumberArray(aids)) throw new Error('listApplications outcome was not an array of numbers');
			return aids;
		} catch (e) {
			throw e;
		}
	}
	
	public async selectApplication(aid: number[]): Promise<boolean> {
		try {
			await (this.tag as MifareDesfireTag).selectApplication(Buffer.from(aid));
			return true;
		} catch (e) {
			CommonsOutput.error(e);
			return false;
		}
	}
	
	public async authenticateDES(keyNum: number, key: number[]): Promise<boolean> {
		try {
			await (this.tag as MifareDesfireTag).authenticateDES(keyNum, Buffer.from(key));
			return true;
		} catch (e) {
			CommonsOutput.error(e);
			return false;
		}
	}
	
	public async authenticate3DES(keyNum: number, key: number[]): Promise<boolean> {
		try {
			await (this.tag as MifareDesfireTag).authenticate3DES(keyNum, Buffer.from(key));
			return true;
		} catch (e) {
			CommonsOutput.error(e);
			return false;
		}
	}
	
	public async listFiles(): Promise<number[]> {
		try {
			const files: any = await (this.tag as MifareDesfireTag).getFileIds();
			if (!CommonsType.isNumberArray(files)) throw new Error('listFiles outcome was not an array of numbers');
			return files;
		} catch (e) {
			throw e;
		}
	}
	
	public async read(file: number, offset: number, length: number): Promise<Buffer> {
		try {
			const read: any = await (this.tag as MifareDesfireTag).read(file, offset, length);
			if (!read || read === null || read === -1 || read === false || read === [] || !CommonsType.isObject(read)) throw new Error('Error reading file');
			if (!(read instanceof Buffer)) throw new Error('Error reading file');

			return read;
		} catch (e) {
			throw e;
		}
	}
	
	public async readAsString(file: INfcDesfireFile): Promise<string> {
		if (!await this.selectApplication(file.aid)) throw new Error('Unable to select application');
		if (!await this.authenticate3DES(file.keyNum, file.key)) throw new Error('Unable to authenticate');
		
		const read: Buffer = await this.read(file.file, file.offset, file.length);
		
		const data: string = ab2str(read);
		return data;
	}
}
