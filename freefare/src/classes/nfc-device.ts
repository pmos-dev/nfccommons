import { Device as FreefareLibDevice } from 'freefare-pseudo';

import { CommonsType } from 'tscommons-core';

import { CommonsOutput } from 'nodecommons-cli';

import { INfcDevice } from 'nfccommons-core';

import { NfcTag } from './nfc-tag';
import { NfcDesfire } from './nfc-desfire';

export class NfcDevice implements INfcDevice {
	public name: string;
	
	constructor(
			private device: FreefareLibDevice
	) {
		this.name = this.device.name;
	}

	public async open(): Promise<boolean> {
		try {
			await this.device.open();
			return true;
		} catch (e) {
			return false;
		}
	}

	public async close(): Promise<boolean> {
		try {
			await this.device.close();
			return true;
		} catch (e) {
			return false;
		}
	}

	public async abort(): Promise<boolean> {
		try {
			await this.device.abort();
			return true;
		} catch (e) {
			return false;
		}
	}
	
	public async listTags(): Promise<NfcTag[]> {
		try {
			const tags: any = await this.device.listTags();
			if (!Array.isArray(tags)) throw new Error('listDevices returned a non-array');
			if (tags.map((d: any): boolean => CommonsType.isObject(d)).filter((d: boolean): boolean => d).length !== tags.length) throw new Error('listTags returned non-object array');

			return tags
					.map((d: any): NfcTag|null => {
						switch (d.getType()) {
							case 'MIFARE_DESFIRE':
								return new NfcDesfire(d);
							default:
								// unsupported type
								CommonsOutput.error(`Unsupported tag type: ${d.getType()}`);
								return null;
						}
					})
					.filter((t: NfcTag|null): boolean => t !== null)
					.map((t: NfcTag|null): NfcTag => t as NfcTag);
		} catch (e) {
			throw e;
		}
	}
}
