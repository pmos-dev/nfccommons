import * as LibFreefare from 'freefare';

import { CommonsType } from 'tscommons-core';

import { NfcDevice } from './nfc-device';

export class NfcFreefare {
	constructor(protected freefare: LibFreefare) {}
	
	public async listDevices(): Promise<NfcDevice[]> {
		try {
			const devices: any = await this.freefare.listDevices();
			if (!Array.isArray(devices)) throw new Error('listDevices returned a non-array');
			if (devices.map((d: any): boolean => CommonsType.isObject(d)).filter((d: boolean): boolean => d).length !== devices.length) throw new Error('listDevices returned non-object array');
			
			return devices.map((d: any): NfcDevice => new NfcDevice(d));
		} catch (e) {
			throw e;
		}
	}
}
