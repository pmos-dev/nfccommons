import {
		MifareUltralightTag,
		MifareClassicTag,
		MifareDesfireTag
} from 'freefare-pseudo';

import { INfcTag, ENfcType } from 'nfccommons-core';

export abstract class NfcTag implements INfcTag {
	public type: ENfcType;
	public friendlyName: string;
	public uid: string;
	
	constructor(
			protected tag: (MifareUltralightTag|MifareClassicTag|MifareDesfireTag)
	) {
		switch (this.tag.getType()) {
			case 'MIFARE_CLASSIC_1K': { this.type = ENfcType.MIFARE_CLASSIC_1K; break; }
			case 'MIFARE_CLASSIC_4K': { this.type = ENfcType.MIFARE_CLASSIC_4K; break; }
			case 'MIFARE_DESFIRE': { this.type = ENfcType.MIFARE_DESFIRE; break; }
			case 'MIFARE_ULTRALIGHT': { this.type = ENfcType.MIFARE_ULTRALIGHT; break; }
			case 'MIFARE_ULTRALIGHT_C': { this.type = ENfcType.MIFARE_ULTRALIGHT_C; break; }
			default:
				throw new Error(`Unknown tag type: ${this.tag.getType()}`);
		}

		this.friendlyName = this.tag.getFriendlyName();
		this.uid = this.tag.getUID();
	}

	public async open(): Promise<boolean> {
		try {
			await this.tag.open();
			return true;
		} catch (e) {
			return false;
		}
	}

	public async close(): Promise<boolean> {
		try {
			await this.tag.close();
			return true;
		} catch (e) {
			return false;
		}
	}
}
