import { CommonsType } from 'tscommons-core';

export interface INfcDesfireFile {
		aid: number[];
		auth: string;
		key: number[];
		keyNum: number;
		file: number;
		offset: number;
		length: number;
}

export function isINfcDesfireFile(test: any): test is INfcDesfireFile {
	if (!CommonsType.isObject(test)) return false;

	const attempt: INfcDesfireFile = test as INfcDesfireFile;
	
	if (!CommonsType.hasPropertyNumberArray(attempt, 'aid')) return false;
	if (!CommonsType.hasPropertyString(attempt, 'auth')) return false;
	if (!CommonsType.hasPropertyNumberArray(attempt, 'key')) return false;
	if (!CommonsType.hasPropertyNumber(attempt, 'keyNum')) return false;
	if (!CommonsType.hasPropertyNumber(attempt, 'file')) return false;
	if (!CommonsType.hasPropertyNumber(attempt, 'offset')) return false;
	if (!CommonsType.hasPropertyNumber(attempt, 'length')) return false;
	
	return true;
}
