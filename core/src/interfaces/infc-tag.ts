import { ENfcType } from '../enums/enfc-type';

export interface INfcTag {
		type: ENfcType;
		friendlyName: string;
		uid: string;
}
