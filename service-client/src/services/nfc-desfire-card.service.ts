import { Observable, Subject } from 'rxjs';

import { CommonsOutput } from 'nodecommons-cli';
import { CommonsIpcUnixSocketAutoReconnectClient } from 'nodecommons-process';

export class NfcDesfireCardService extends CommonsIpcUnixSocketAutoReconnectClient {
	private onFailed: Subject<void> = new Subject<void>();
	private onConnectedStateChange: Subject<boolean> = new Subject<boolean>();
	private onDetected: Subject<string> = new Subject<string>();

	constructor() {
		super(
				'nfc-desfire-card-service',
				20,
				2000,			// reconnect delay
				1000,			// noop interval
				'_noop',		// noop command
				(): void => {	// failure callback
					this.onFailed.next();
				},
				(state: boolean): void => {	// connected callback
					this.onConnectedStateChange.next(state);
				},
				false	// don't require an initial connection; allow to poll for 20 times first before aborting
		);
	}
	
	public failedObservable(): Observable<void> {
		return this.onFailed;
	}
	
	public connectedStateChangeObservable(): Observable<boolean> {
		return this.onConnectedStateChange;
	}
	
	public detectedObservable(): Observable<string> {
		return this.onDetected;
	}
	
	public async start(): Promise<boolean> {
		CommonsOutput.doing('Connecting to NFC DESFire card scanner service');
		
		try {
			await this.listen(
					(data: string): void => {
						const regex: RegExp = /^DETECTED ([a-z0-9]+) (.+)$/;
						const result: RegExpExecArray|null = regex.exec(data);
						
						if (result === null) {
							CommonsOutput.error('Invalid detection data received from IPC server');
							return;
						}

						this.onDetected.next(result[2]);
					}
			);

			CommonsOutput.success();
			return true;
		} catch (e) {
			CommonsOutput.fail();
			return false;
		}
	}
	
	public async enable(): Promise<void> {
		await this.send('ENABLE');
	}
	
	public async disable(): Promise<void> {
		await this.send('DISABLE');
	}
	
	public abortServer(): void {
		this.send('ABORT');	// don't await
	}
}
