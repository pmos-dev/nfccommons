// This is a helper API. It isn't actually used by this package itself.

import * as express from 'express';

import { CommonsType } from 'tscommons-core';
import { TPropertyObject } from 'tscommons-core';
import { CommonsObject } from 'tscommons-core';

import { CommonsRestServer } from 'nodecommons-rest';
import { CommonsRestApi } from 'nodecommons-rest';

// NB we can't use observables for the various methods as we have to be able to await on them

export class NfcScannerApi extends CommonsRestApi {
	constructor(
			restServer: CommonsRestServer,
			path: string,
			isServiceUpCallback: () => Promise<boolean>,
			doScannerEnabled: (_: boolean) => Promise<void>,
			doScannerAbort: () => Promise<void>
	) {
		super(restServer);
		
		super.getHandler(
				`${path}scanner/service`,
				async (_req: express.Request, _res: express.Response): Promise<boolean> => {
					return await isServiceUpCallback();
				}
		);
		
		super.patchHandler(
				`${path}scanner/enabled`,
				async (req: express.Request, _res: express.Response): Promise<void> => {
					if (!CommonsType.hasPropertyObject(req, 'body')) return CommonsRestApi.badRequest('No body supplied');
					const object: TPropertyObject = CommonsObject.stripNulls(CommonsType.decodePropertyObject(req.body));
					
					if (!CommonsType.hasProperty(object, 'state')) return CommonsRestApi.badRequest('No state specified');
					const state: boolean = CommonsType.attemptBoolean(object.state) || false;

					await doScannerEnabled(state);
				}
		);
		
		super.deleteHandler(
				`${path}scanner/service`,
				async (_req: express.Request, _res: express.Response): Promise<void> => {
					await doScannerAbort();
				}
		);
	}
}
